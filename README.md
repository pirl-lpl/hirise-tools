HiRISE-Specific Tools
=====================

This repo is a collection of HiRISE-specific tools. These tools are compact,
self-contained tools with relatively few external dependencies and no build
step.

Needed
------

It would be nice to have a *very simple* installer of some kind for these tools,
one that simply copies them all to a user-designated directory, maybe also
updating the user's executable path as needed in whichever shell config file
is in use (e.g., `.cshrc` or `.bash` or whatever).
